import express from "express";
import "express-async-errors";
import { json } from "body-parser";
import cookieSession from "cookie-session";
import { NotFoundError } from "../errors/not-found-error";
import { findAllBookRouter } from "./routes/bookService/findAll";
import { insertBookRouter } from "./routes/bookService/insert";
import { findByIdBookRouter } from "./routes/bookService/findById";
import { saveUserRouter } from "./routes/authService/signup";
import { signinUserRouter } from "./routes/authService/signin";
import { saveAdminRouter } from "./routes/authService/signupAdmin";
import { signoutUserRouter } from "./routes/authService/signout";
import { currentUserRouter } from "./routes/authService/currentUser";
import { insertOrderRouter } from "./routes/orderService/saveOrder";
import { findOrderItemsRouter } from "./routes/orderService/orderItems";
import { deleteOrderRouter } from "./routes/orderService/deleteOrder";
import { checkoutRouter } from "./routes/orderService/checkout";
import { deleteAllOrderRouter } from "./routes/orderService/deleteAll";

const app = express();
app.set("trust proxy", true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== "test",
  })
);

if (!process.env.CURRENT_USER_ENDPOINT) {
  throw new Error("CURRENT_USER_ENDPOINT undifined");
}

if (!process.env.SIGNUP_USER_ENDPOINT) {
  throw new Error("SIGNUP_USER_ENDPOINT undifined");
}

if (!process.env.SIGNIN_USER_ENDPOINT) {
  throw new Error("SIGNIN_USER_ENDPOINT undifined");
}

if (!process.env.SIGNOUT_USER_ENDPOINT) {
  throw new Error("SIGNOUT_USER_ENDPOINT undifined");
}

if (!process.env.SIGNUP_ADMIN_ENDPOINT) {
  throw new Error("SIGNUP_ADMIN_ENDPOINT undifined");
}

app.use(findAllBookRouter);
app.use(insertBookRouter);
app.use(findByIdBookRouter);
app.use(signinUserRouter);

app.use(saveUserRouter);
app.use(signinUserRouter);
app.use(saveAdminRouter);
app.use(signoutUserRouter);
app.use(currentUserRouter);

app.use(insertOrderRouter);
app.use(findOrderItemsRouter);
app.use(deleteOrderRouter);
app.use(checkoutRouter);
app.use(deleteAllOrderRouter);

app.all("*", async (req, res) => {
  throw new NotFoundError();
});

export { app };
