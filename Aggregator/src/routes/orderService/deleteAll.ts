import express, { Request, Response } from "express";
import axios from "axios";

const router = express.Router();

router.delete(
  "/api/aggregator/orders/",
  async (req: Request, res: Response) => {
    const response = await axios["delete"](`http://order-srv:3000/api/orders`, {
      headers: {
        Cookie: `micro=${req.headers.cookie?.split("=")[1]}`,
      },
    });
    res.send(response.data);
  }
);

export { router as deleteAllOrderRouter };
