import express, { Request, Response } from "express";
import axios from "axios";

const router = express.Router();

router.get("/api/aggregator/books/:id", async (req: Request, res: Response) => {
  console.log(req.params);
  const response = await axios["get"](
    `http://book-srv:3000/api/books/${req.params.id}`,
    { withCredentials: true }
  );
  console.log(response.headers["set-cookie"]);
  res.send(response.data);
});

export { router as findByIdBookRouter };
