import express, { Request, Response } from "express";
import axios from "axios";

const router = express.Router();

router.post("/api/aggregator/books", async (req: Request, res: Response) => {
  console.log(req);
  const response = await axios["post"](
    "http://book-srv:3000/api/books",
    req.body,
    {
      headers: {
        Cookie: `micro=${req.headers.cookie?.split("o=")[1]}`,
      },
    }
  );

  console.log(response.data);
  res.send(response.data);
});

export { router as insertBookRouter };
