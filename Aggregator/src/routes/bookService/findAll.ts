import express, { Request, Response } from "express";
import axios from "axios";

const router = express.Router();

router.get("/api/aggregator/books", async (req: Request, res: Response) => {
  const response = await axios["get"]("http://book-srv:3000/api/books");
  res.send(response.data);
});

export { router as findAllBookRouter };
