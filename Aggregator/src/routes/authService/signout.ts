import express, { Request, Response } from "express";
import axios from "axios";

const router = express.Router();

router.post(
  "/api/aggregator/auth/signout",
  async (req: Request, res: Response) => {
    const response = await axios["post"](
      `${process.env.SIGNOUT_USER_ENDPOINT}`,
      req.body
    );

    if (response.headers["set-cookie"][1]) {
      res.clearCookie("admin");
    }

    res.clearCookie("micro");

    res.send(response.data);
  }
);

export { router as signoutUserRouter };
