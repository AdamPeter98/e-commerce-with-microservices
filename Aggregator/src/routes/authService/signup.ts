import express, { Request, Response } from "express";
import axios from "axios";

const router = express.Router();

router.post(
  "/api/aggregator/auth/singup",
  async (req: Request, res: Response) => {
    const response = await axios["post"](
      `${process.env.SIGNUP_USER_ENDPOINT}`,
      req.body
    );

    const cookie: string = response.headers["set-cookie"][0]
      .split(";")[0]
      .split("=")[1];
    res.cookie("micro", cookie);
    res.send(response.data);
  }
);

export { router as saveUserRouter };
