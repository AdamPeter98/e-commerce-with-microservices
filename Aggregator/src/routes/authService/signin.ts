import express, { Request, Response } from "express";
import axios from "axios";

const router = express.Router();

router.post(
  "/api/aggregator/auth/signin",
  async (req: Request, res: Response) => {
    const response = await axios["post"](
      `${process.env.SIGNIN_USER_ENDPOINT}`,
      req.body
    );

    if (response.headers["set-cookie"][1]) {
      const cookie: string = response.headers["set-cookie"][1]
        .split(";")[0]
        .split("=")[1];
      res.cookie("micro", cookie);
      res.cookie("admin", "admin");
    } else {
      const cookie: string = response.headers["set-cookie"][0]
        .split(";")[0]
        .split("=")[1];
      res.cookie("micro", cookie);
    }
    res.send(response.data);
  }
);

export { router as signinUserRouter };
