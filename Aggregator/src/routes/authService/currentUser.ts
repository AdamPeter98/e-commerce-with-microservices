import express, { Request, Response } from "express";
import axios from "axios";

const router = express.Router();

router.get(
  "/api/aggregator/auth/currentuser",
  async (req: Request, res: Response) => {
    let jwt;
    let admin;
    let response;

    if (req.headers.cookie?.includes("admin")) {
      jwt = `micro=${req.headers.cookie?.split("o=")[1]}`;
      admin = "admin=admin";

      response = await axios["get"](`${process.env.CURRENT_USER_ENDPOINT}`, {
        headers: {
          Cookie: `${jwt}; ${admin}`,
        },
      });
    } else if (
      !req.headers.cookie?.includes("admin") &&
      req.headers.cookie?.includes("micro")
    ) {
      jwt = `micro=${req.headers.cookie?.split("=")[1]}`;
      response = await axios["get"](`${process.env.CURRENT_USER_ENDPOINT}`, {
        headers: {
          Cookie: jwt,
        },
      });
    } else {
      response = await axios["get"](`${process.env.CURRENT_USER_ENDPOINT}`);
    }
    res.send(response.data);
  }
);

export { router as currentUserRouter };
