import express, { Request, Response } from "express";
import axios from "axios";

const router = express.Router();

router.post(
  "/api/aggregator/auth/admin/singup",
  async (req: Request, res: Response) => {
    const response = await axios["post"](
      `${process.env.SIGNUP_ADMIN_ENDPOINT}`,
      req.body
    );

    const cookie: string = response.headers["set-cookie"][1]
      .split("o=")[1]
      .split(";")[0];

    res.cookie("micro", cookie);
    res.cookie("admin", "admin");
    res.send(response.data);
  }
);

export { router as saveAdminRouter };
