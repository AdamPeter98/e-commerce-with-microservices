import Router from "next/router";
import useRequest from "../../hooks/use-request";

const BookShow = ({ book }) => {
  const { doRequest, errors } = useRequest({
    url: "/api/aggregator/orders",
    method: "post",
    body: {
      bookId: book.id,
    },
    onSuccess: (order) => Router.push("/", `/`),
  });

  return (
    <div>
      <div>
        <br />
      </div>
      <img
        class="card-img-top img-responsive"
        src={`/${book.imageURL}`}
        style={{ height: 170, width: 90 }}
      />
      <div>
        <br />
      </div>
      <h1>{book.title}</h1>
      <h4> {book.description}</h4>
      <div>
        <br />
      </div>
      <h4>Price: {book.price}</h4>
      {errors}
      <div>
        <br />
      </div>
      <button onClick={() => doRequest()} className="btn btn-primary">
        Add Cart
      </button>
    </div>
  );
};

BookShow.getInitialProps = async (context, client) => {
  const { bookId } = context.query;
  const { data } = await client.get(`/api/aggregator/books/${bookId}`);

  return { book: data };
};

export default BookShow;
