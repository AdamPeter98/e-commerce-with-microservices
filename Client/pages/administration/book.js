import { useState, useEffect } from "react";
import useRequest from "../../hooks/use-request";

export default () => {
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");
  const [imageURL, setImageURL] = useState("");
  const [unitsInStock, setUnitsInStock] = useState("");
  const { doRequest, errors } = useRequest({
    url: "/api/aggregator/books",
    method: "post",
    body: {
      title,
      price,
      imageURL,
      unitsInStock,
    },
    onSuccess: () => Router.push("/"),
  });

  const onSubmit = async (event) => {
    event.preventDefault();

    await doRequest();
  };

  return (
    <form>
      <h1>Add book</h1>
      <div className="form-group">
        <label>Title</label>
        <input
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          className="form-control"
        />
      </div>
      <div className="form-group">
        <label>Price</label>
        <input
          value={price}
          onChange={(e) => setPrice(e.target.value)}
          className="form-control"
        />
      </div>
      <div className="form-group">
        <label>image URL</label>
        <input
          value={imageURL}
          onChange={(e) => setImageURL(e.target.value)}
          className="form-control"
        />
      </div>
      <div className="form-group">
        <label>Units In Stock</label>
        <input
          value={unitsInStock}
          onChange={(e) => setUnitsInStock(e.target.value)}
          className="form-control"
        />
      </div>
      {errors}
      <div>
        <br />
      </div>
      <button onClick={onSubmit} className="btn btn-primary">
        Add book
      </button>
    </form>
  );
};
