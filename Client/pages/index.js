import Link from "next/link";

const LandingPage = ({ currentUser, books }) => {
  const bookList = books.map((book) => {
    return (
      <tr key={book.id}>
        <td>
          <img
            class="card-img-top img-responsive"
            src={`/${book.imageURL}`}
            style={{ height: 170, width: 110 }}
          />
        </td>

        <h6 class="card-title">
          <td>{book.title}</td>
        </h6>
        <h7>
          <td>{book.price}</td>
        </h7>
        <p class="card-text">
          <td>{book.description}</td>
        </p>

        <td>
          <Link href="/books/[bookId]" as={`/books/${book.id}`}>
            <a>Details</a>
          </Link>
        </td>
        <div>
          <br />
        </div>
      </tr>
    );
  });

  return (
    <div class="row">
      <div>
        <h1>Books</h1>
        <div>
          <br />
        </div>
        <table className="table" style={{ width: "100%" }}>
          <tbody>{bookList}</tbody>
        </table>
      </div>
    </div>
  );
};
LandingPage.getInitialProps = async (context, client, currentUser) => {
  const { data } = await client.get("/api/aggregator/books");
  return { books: data };
};

export default LandingPage;
