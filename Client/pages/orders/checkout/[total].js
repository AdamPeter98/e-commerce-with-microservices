import { useState, useEffect } from "react";
import Router from "next/router";
import useRequest from "../../../hooks/use-request";

const TotalShow = ({ currentUser, total }) => {
  const [city, setCity] = useState("");
  const [country, setCountry] = useState("");
  const [streetAndNumber, setStreetAndNumber] = useState("");

  const { doRequest, errors } = useRequest({
    url: "/api/aggregator/orders/checkout",
    method: "post",
    body: {
      city,
      country,
      streetAndNumber,
      userEmail: currentUser,
      total: total,
    },
    onSuccess: () => Router.push("/orders/removeOrder", "/orders/removeOrder"),
  });

  const onSubmit = async (event) => {
    event.preventDefault();

    await doRequest();
  };

  return (
    <form onSubmit={onSubmit}>
      <h1>Checkout</h1>
      <div className="form-group">
        <label>City</label>
        <input
          value={city}
          onChange={(e) => setCity(e.target.value)}
          className="form-control"
        />
      </div>
      <div className="form-group">
        <label>Country</label>
        <input
          value={country}
          onChange={(e) => setCountry(e.target.value)}
          className="form-control"
        />
      </div>
      <div className="form-group">
        <label>Street and Number</label>
        <input
          value={streetAndNumber}
          onChange={(e) => setStreetAndNumber(e.target.value)}
          className="form-control"
        />
      </div>
      <div>
        <br></br>
      </div>
      <button className="btn btn-primary">Finalize</button>
    </form>
  );
};

TotalShow.getInitialProps = async (context, client, currentUser) => {
  const { total } = context.query;
  return { currentUser: currentUser, total: total };
};

export default TotalShow;
