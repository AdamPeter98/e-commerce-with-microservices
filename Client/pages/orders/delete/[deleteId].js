import router from "next/router";

const DeleteShow = () => {
  router.push("/orders");
  return <h1>Deleted</h1>;
};

DeleteShow.getInitialProps = async (context, client) => {
  const { deleteId } = context.query;

  await client.delete(`/api/aggregator/orders/${deleteId}`);
};

export default DeleteShow;
