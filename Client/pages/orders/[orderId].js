import router from "next/router";

const DeleteShow = () => {
  router.push("/");
  return <h1></h1>;
};

DeleteShow.getInitialProps = async (context, client) => {
  await client.delete(`/api/aggregator/orders/`);
};

export default DeleteShow;
