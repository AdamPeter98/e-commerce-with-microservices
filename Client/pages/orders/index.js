import Router from "next/router";
import useRequest from "../../hooks/use-request";
const OrderIndex = ({ orders }) => {
  let total = 0;
  orders.map((order) => {
    total = total + order.book.price;
  });

  const { doRequest, errors } = useRequest({
    url: "/api/aggregator/books",
    method: "get",
    onSuccess: () =>
      Router.push("/orders/checkout/[total]", `/orders/checkout/${total}`),
  });

  const bookList = orders.map((order) => {
    console.log(order.id);
    const { doRequest, errors } = useRequest({
      url: "/api/aggregator/books",
      method: "get",
      onSuccess: () =>
        Router.push("/orders/delete/[deleteId]", `/orders/delete/${order.id}`),
    });

    return (
      <tr>
        <td>
          <img
            class="card-img-top img-responsive"
            src={`/${order.book.imageURL}`}
            style={{ height: 170, width: 110 }}
          />
        </td>

        <h6 class="card-title">
          <td>
            <p>{order.book.title}</p>
          </td>
        </h6>
        <h7>
          <td>
            <p>{order.book.price}</p>
          </td>
        </h7>

        <td>
          <button
            onClick={() => doRequest()}
            class="btn btn-primary btn-sm float-right"
          >
            Remove
          </button>
        </td>
      </tr>
    );
  });

  return (
    <div class="row">
      <table>
        <div>
          <br />
        </div>
        <thead>
          <tr>
            <td width="20%">My Items</td>
          </tr>
        </thead>
        <div>
          <br />
        </div>
        <tbody>
          {bookList}
          <tr>
            <td colspan="2"></td>
            <td class="font-weight-bold">
              <p>Shipping : Free</p>
              <p>Total price : {total}</p>
              <button
                type="button"
                onClick={() => doRequest()}
                class="btn btn-primary"
              >
                Checkout
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

OrderIndex.getInitialProps = async (context, client) => {
  const { data } = await client.get("/api/aggregator/orders");
  return { orders: data, clientT: client };
};

export default OrderIndex;
