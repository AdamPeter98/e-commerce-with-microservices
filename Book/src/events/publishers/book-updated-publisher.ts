import { Publisher } from "../base-publisher";
import { Subjects } from "../subjects";
import { BookUpdatedEvent } from "../book-updated-event";

export class BookUpdatedPublisher extends Publisher<BookUpdatedEvent> {
  subject: Subjects.BookUpdated = Subjects.BookUpdated;
}
