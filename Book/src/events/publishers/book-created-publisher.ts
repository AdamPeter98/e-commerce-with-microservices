import { Publisher } from "../base-publisher";
import { Subjects } from "../subjects";
import { BookCreatedEvent } from "../book-created-event";
export class BookCreatedPublisher extends Publisher<BookCreatedEvent> {
  subject: Subjects.BookCreated = Subjects.BookCreated;
}
