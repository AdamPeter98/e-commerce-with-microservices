export enum Subjects {
  OrderCreated = "order:created",
  OrderCancelled = "order:cancelled",
  BookCreated = "book:created",
  BookUpdated = "book:updated",
}
