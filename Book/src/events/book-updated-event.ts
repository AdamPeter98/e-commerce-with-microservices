import { Subjects } from "./subjects";

export interface BookUpdatedEvent {
  subject: Subjects.BookUpdated;
  data: {
    id: string;
    createdByAdmin: any;
    title: any;
    price: number;
    imageURL: any;
    description: any;
    unitsInStock: any;
  };
}
