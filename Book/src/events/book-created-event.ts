import { Subjects } from "./subjects";

export interface BookCreatedEvent {
  subject: Subjects.BookCreated;
  data: {
    id: string;
    createdByAdmin: any;
    title: any;
    price: number;
    imageURL: any;
    description: any;
    unitsInStock: any;
  };
}
