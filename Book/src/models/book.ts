import mongoose from "mongoose";

interface BookAttrs {
  title: string;
  price: number;
  createdByAdmin: any;
  description: any;
  imageURL: any;
  unitsInStock: any;
}

interface BookDoc extends mongoose.Document {
  title: String;
  price: number;
  createdByAdmin: any;
  imageURL: any;
  orderId?: any;
  description: any;
  unitsInStock: any;
}

interface BookModel extends mongoose.Model<BookDoc> {
  build(attrs: BookAttrs): BookDoc;
}

const bookSchema = new mongoose.Schema(
  {
    title: {
      type: String,
    },
    price: {
      type: Number,
    },
    createdByAdmin: {
      type: String,
    },
    imageURL: {
      type: String,
    },
    description: {
      type: String,
    },
    orderId: {
      type: String,
    },
    unitsInStock: {
      type: Number,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

bookSchema.statics.build = (attrs: BookAttrs) => {
  return new Book(attrs);
};

const Book = mongoose.model<BookDoc, BookModel>("Book", bookSchema);

export { Book };
