import express from "express";
import "express-async-errors";
import { json } from "body-parser";
import cookieSession from "cookie-session";
import { NotFoundError } from "./errors/not-found-error";
import { currentUser } from "./middlewares/current-user";
import { indexBookRouter } from "./routes";
import { createBookRouter } from "./routes/new";
import { showBookRouter } from "./routes/show";
import { updateBookRouter } from "./routes/update";

const app = express();
app.set("trust proxy", true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== "test",
  })
);
app.use(currentUser);

app.use(indexBookRouter);
app.use(createBookRouter);
app.use(showBookRouter);
app.use(updateBookRouter);

app.all("*", async (req, res) => {
  throw new NotFoundError();
});

export { app };
