import express, { Request, Response } from "express";
import { NotFoundError } from "../errors/not-found-error";
import { Book } from "../models/book";

const router = express.Router();

const redis = require("redis");
const client = redis.createClient({
  host: process.env.REDIS_HOST,
  port: 6379,
});

client.on("connect", () => {
  console.log("client connected to redis");
});

client.on("error", (err: any) => {
  console.log(err);
});

let book: any;

router.get("/api/books/:id", async (req: Request, res: Response) => {
  client.GET(req.params.id, async (err: any, value: any) => {
    if (err) {
      console.log(err);
    } else if (value) {
      console.log(value, " from redis");
      book = JSON.parse(value);
    } else {
      console.log("data from database");
      book = await Book.findById(req.params.id);
    }
  });

  if (!book) {
    throw new NotFoundError();
  }

  res.send(book);
});

export { router as showBookRouter };
