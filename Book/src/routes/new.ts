import express, { Request, Response } from "express";
import { body } from "express-validator";
import { currentUser } from "../middlewares/current-user";
import { requireAuth } from "../middlewares/require-auth";
import { Book } from "../models/book";
import { natsWrapper } from "../nats-wrapper";
import { BookCreatedPublisher } from "../events/publishers/book-created-publisher";

const router = express.Router();

const redis = require("redis");
const client = redis.createClient({
  host: process.env.REDIS_HOST,
  port: 6379,
});

client.on("connect", () => {
  console.log("client connected to redis");
});

client.on("error", (err: any) => {
  console.log(err);
});

router.post(
  "/api/books",
  currentUser,
  requireAuth,
  [
    body("title").not().isEmpty().withMessage("Title is required"),
    body("price")
      .isFloat({ gt: 0 })
      .withMessage("Price must be greater than 0"),
    body("unitsInStock").not().isEmpty().withMessage("unitsInStock required"),
  ],
  async (req: Request, res: Response) => {
    const { title, price, imageURL, unitsInStock, description } = req.body;

    const book = Book.build({
      title,
      price,
      createdByAdmin: req.currentUser!.sub,
      imageURL,
      description,
      unitsInStock,
    });
    await book.save();

    client.SET(book.id, JSON.stringify(book));

    await new BookCreatedPublisher(natsWrapper.client).publish({
      id: book.id,
      title: book.title,
      price: book.price,
      createdByAdmin: book.createdByAdmin,
      imageURL: book.imageURL,
      description: book.description,
      unitsInStock: book.unitsInStock,
    });

    client.GET(book.id, (err: any, value: any) => {
      if (err) {
        console.log(err.mesaage);
      } else {
        console.log(value);
      }
    });

    res.status(201).send(book);
  }
);

export { router as createBookRouter };
