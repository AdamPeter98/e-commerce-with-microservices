import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest } from "../middlewares/validate-request";
import { NotFoundError } from "../errors/not-found-error";
import { requireAuth } from "../middlewares/require-auth";
import { natsWrapper } from "../nats-wrapper";
import { Book } from "../models/book";
import { BookUpdatedPublisher } from "../events/publishers/book-updated-publisher";
const router = express.Router();

router.put(
  "/api/books/:id",
  requireAuth,
  [
    body("title").not().isEmpty().withMessage("Title is required"),
    body("price")
      .isFloat({ gt: 0 })
      .withMessage("Price must be provided and must be greater than 0"),
    body("imageURL").not().isEmpty().withMessage("imageURL  is required"),
    body("unitsInStock")
      .not()
      .isEmpty()
      .withMessage("unitsInStock is required"),
    body("description").not().isEmpty().withMessage("description is required"),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const book = await Book.findById(req.params.id);

    if (!book) {
      throw new NotFoundError();
    }

    book.set({
      title: req.body.title,
      price: req.body.price,
      imageURL: req.body.imageURL,
      description: req.body.description,
      unitsInStock: req.body.unitsInStock,
    });
    await book.save();

    await new BookUpdatedPublisher(natsWrapper.client).publish({
      id: book.id,
      title: book.title,
      createdByAdmin: book.createdByAdmin,
      price: book.price,
      imageURL: book.imageURL,
      description: book.description,
      unitsInStock: book.unitsInStock,
    });

    res.send(book);
  }
);

export { router as updateBookRouter };
