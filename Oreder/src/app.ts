import express from "express";
import "express-async-errors";
import { json } from "body-parser";
import cookieSession from "cookie-session";
import { NotFoundError } from "../errors/not-found-error";
import { currentUser } from "./middlewares/current-user";
import { findAllOrderRouter } from "./routes/findAll";
import { findByIdRouter } from "./routes/findById";
import { insertOrderRouter } from "./routes/insert";
import { deleteOrderRouter } from "./routes/update";
import { removeOrderRouter } from "./routes/deleteById";
import { checkoutOrderRouter } from "./routes/checkout";
import { removeAllOrderRouter } from "./routes/deleteAll";

const app = express();
app.set("trust proxy", true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== "test",
  })
);
app.use(currentUser);
app.use(insertOrderRouter);
app.use(deleteOrderRouter);
app.use(findAllOrderRouter);
app.use(findByIdRouter);
app.use(removeOrderRouter);
app.use(checkoutOrderRouter);
app.use(removeAllOrderRouter);

app.all("*", async (req, res) => {
  throw new NotFoundError();
});

export { app };
