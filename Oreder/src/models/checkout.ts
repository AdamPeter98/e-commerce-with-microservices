import mongoose from "mongoose";

interface CheckoutAttrs {
  city: any;
  country: any;
  streetAndNumber: any;
  userEmail: any;
  total: any;
}

export interface CheckoutDoc extends mongoose.Document {
  city: any;
  country: any;
  streetAndNumber: any;
  userEmail: any;
  total: any;
}

interface CheckoutModel extends mongoose.Model<CheckoutDoc> {
  build(attrs: CheckoutAttrs): CheckoutDoc;
}

const checkoutSchema = new mongoose.Schema(
  {
    city: {
      type: String,
    },
    country: {
      type: String,
    },
    streetAndNumber: {
      type: String,
    },
    userEmail: {
      type: String,
    },
    total: {
      type: Number,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

checkoutSchema.statics.build = (attrs: CheckoutAttrs) => {
  return new Checkout({
    city: attrs.city,
    country: attrs.country,
    streetAndNumber: attrs.streetAndNumber,
    total: attrs.total,
    userEmail: attrs.userEmail,
  });
};

const Checkout = mongoose.model<CheckoutDoc, CheckoutModel>(
  "Checkout",
  checkoutSchema
);

export { Checkout };
