import mongoose from "mongoose";

interface BookAttrs {
  id: any;
  title: any;
  price: any;
  imageURL: any;
}

export interface BookDoc extends mongoose.Document {
  title: any;
  price: any;
  imageURL: any;
}

interface BookModel extends mongoose.Model<BookDoc> {
  build(attrs: BookAttrs): BookDoc;
}

const bookSchema = new mongoose.Schema(
  {
    title: {
      type: String,
    },
    price: {
      type: Number,
      min: 0,
    },
    imageURL: {
      type: String,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

bookSchema.statics.build = (attrs: BookAttrs) => {
  return new Book({
    _id: attrs.id,
    title: attrs.title,
    price: attrs.price,
    imageURL: attrs.imageURL,
  });
};

const Book = mongoose.model<BookDoc, BookModel>("Book", bookSchema);

export { Book };
