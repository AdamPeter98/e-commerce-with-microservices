import mongoose, { mongo } from "mongoose";
import { OrderStatus } from "../../events/types/order-status";
import { BookDoc } from "./book";

interface OrderAttrs {
  userEmail: String;
  status: OrderStatus;
  expiresAt: Date;
  book: BookDoc;
}

interface OrderDoc extends mongoose.Document {
  userEmail: String;
  status: OrderStatus;
  expiresAt: Date;
  book: BookDoc;
}

interface OrderModel extends mongoose.Model<OrderDoc> {
  build(attrs: OrderAttrs): OrderDoc;
}

const orderSchema = new mongoose.Schema(
  {
    userEmail: {
      type: String,
    },
    status: {
      type: String,
      enum: Object.values(OrderStatus),
      default: OrderStatus.Created,
    },
    expiresAt: {
      type: mongoose.Schema.Types.Date,
    },
    book: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Book",
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

orderSchema.statics.build = (attrs: OrderAttrs) => {
  return new Order(attrs);
};

const Order = mongoose.model<OrderDoc, OrderModel>("Order", orderSchema);

export { Order };
