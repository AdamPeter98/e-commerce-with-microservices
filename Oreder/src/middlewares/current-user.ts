import { Request, Response, NextFunction } from "express";
import jwt_decode from "jwt-decode";
interface UserPayload {
  sub: String;
}

declare global {
  namespace Express {
    interface Request {
      currentUser?: UserPayload | undefined;
    }
  }
}

export const currentUser = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.headers.cookie?.split("=")[1]) {
    return next();
  }

  try {
    const payload: UserPayload = jwt_decode(req.headers.cookie?.split("=")[1]);
    req.currentUser = payload;
  } catch (err) {
    console.log(err);
  }

  next();
};
