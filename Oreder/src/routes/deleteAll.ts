import express, { Request, Response } from "express";

import { requireAuth } from "../middlewares/require-auth";
import { Order } from "../models/order";

const router = express.Router();

router.delete(
  "/api/orders/",
  requireAuth,
  async (req: Request, res: Response) => {
    const orders = await Order.find({
      userEmail: req.currentUser?.sub,
    }).populate("order");

    await orders.forEach((element) => {
      element.remove();
    });

    res.status(204).send("Order removed");
  }
);

export { router as removeAllOrderRouter };
