import express, { Request, Response } from "express";
import { requireAuth } from "../middlewares/require-auth";
import { body } from "express-validator";
import { Checkout } from "../models/checkout";

const router = express.Router();

router.post(
  "/api/orders/checkout",
  requireAuth,
  [body("total").not().isEmpty().withMessage("total must be provided")],
  async (req: Request, res: Response) => {
    const { total, city, country, streetAndNumber } = req.body;

    const checkout = Checkout.build({
      city: city,
      country: country,
      streetAndNumber: streetAndNumber,
      userEmail: req.currentUser!.sub,
      total: total,
    });

    await checkout.save();

    res.status(201).send(checkout);
  }
);

export { router as checkoutOrderRouter };
