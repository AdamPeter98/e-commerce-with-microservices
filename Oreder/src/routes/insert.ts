import express, { Request, Response } from "express";
import { requireAuth } from "../middlewares/require-auth";
import { body } from "express-validator";
import { Book } from "../models/book";
import { Order } from "../models/order";
import { NotFoundError } from "../../errors/not-found-error";
import { OrderStatus } from "../../events/types/order-status";
import { OrderCreatedPublisher } from "../../events/publishers/order-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

const EXPIRATION_WINDOW_SECOUNDS = 15 * 60;

router.post(
  "/api/orders",
  requireAuth,
  [body("bookId").not().isEmpty().withMessage("BookId must be provided")],
  async (req: Request, res: Response) => {
    const { bookId } = req.body;

    const book = await Book.findById(bookId);

    if (!bookId) {
      throw new NotFoundError();
    }

    if (!book) {
      throw new NotFoundError();
    }

    const expiration = new Date();
    expiration.setSeconds(expiration.getSeconds() + EXPIRATION_WINDOW_SECOUNDS);

    const order = Order.build({
      userEmail: req.currentUser!.sub,
      status: OrderStatus.Created,
      expiresAt: expiration,
      book: book,
    });

    await order.save();

    new OrderCreatedPublisher(natsWrapper.client).publish({
      id: order.id,
      status: order.status,
      userEmail: order.userEmail,
      expiresAt: order.expiresAt.toISOString(),
      ticket: {
        id: book.id,
        price: book.price,
      },
    });

    res.status(201).send(order);
  }
);

export { router as insertOrderRouter };
