import express, { Request, Response } from "express";
import { NotAuthorizedError } from "../../errors/not-authorized-error";
import { NotFoundError } from "../../errors/not-found-error";
import { OrderStatus } from "../../events/types/order-status";
import { requireAuth } from "../middlewares/require-auth";
import { Order } from "../models/order";

const router = express.Router();

router.delete(
  "/api/orders/:orderId",
  requireAuth,
  async (req: Request, res: Response) => {
    const { orderId } = req.params;

    console.log(req.params);

    const order = await Order.findById(orderId).populate("order");

    console.log(order);

    if (!orderId) {
      throw new NotFoundError();
    }

    if (order?.userEmail !== req.currentUser?.sub) {
      throw new NotAuthorizedError();
    }

    order!.status = OrderStatus.Cancelled;
    await order?.remove();

    res.status(204).send("Order removed");
  }
);

export { router as removeOrderRouter };
