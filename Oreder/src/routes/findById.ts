import express, { Request, Response } from "express";
import { NotAuthorizedError } from "../../errors/not-authorized-error";
import { NotFoundError } from "../errors/not-found-error";
import { requireAuth } from "../middlewares/require-auth";
import { Order } from "../models/order";

const router = express.Router();

router.get(
  "/api/orders/:orderId",
  requireAuth,
  async (req: Request, res: Response) => {
    const order = await Order.findById(req.params.orderId).populate("order");
    if (!order) {
      throw new NotFoundError();
    }

    if (order.userEmail !== req.currentUser?.sub) {
      throw new NotAuthorizedError();
    }

    res.send(order);
  }
);

export { router as findByIdRouter };
