import express, { Request, Response } from "express";
import { requireAuth } from "../middlewares/require-auth";
import { Order } from "../models/order";

const router = express.Router();

router.get("/api/orders", requireAuth, async (req: Request, res: Response) => {
  const orders = await Order.find({
    userEmail: req.currentUser!.sub,
  }).populate("book");

  res.send(orders);
});

export { router as findAllOrderRouter };
