export enum OrderStatus {
  Created = "created",
  Cancelled = "cancelled",
  AwaitingPayamnet = "awaiting:payment",
  Complete = "complete",
}
