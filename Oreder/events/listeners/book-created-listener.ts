import { Message } from "node-nats-streaming";
import { Listener } from "../base-listener";
import { Subjects } from "../subjects";
import { queueGroupName } from "./queue-grop-name";
import { BookCreatedEvent } from "../book-created-event";
import { Book } from "../../src/models/book";

export class BookCreatedListener extends Listener<BookCreatedEvent> {
  subject: Subjects.BookCreated = Subjects.BookCreated;
  queueGroupName = queueGroupName;

  async onMessage(data: BookCreatedEvent["data"], msg: Message) {
    const { id, title, price, imageURL } = data;

    console.log(data);

    const book = Book.build({
      id,
      title,
      price,
      imageURL,
    });

    await book.save();

    msg.ack();
  }
}
