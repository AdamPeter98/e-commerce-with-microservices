import { Subjects } from "./subjects";
import { OrderStatus } from "./types/order-status";

export interface OrderCreatedEvent {
  subject: Subjects.OrderCreated;
  data: {
    id: any;
    status: OrderStatus;
    userEmail: any;
    expiresAt: string;
    ticket: {
      id: string;
      price: number;
    };
  };
}
