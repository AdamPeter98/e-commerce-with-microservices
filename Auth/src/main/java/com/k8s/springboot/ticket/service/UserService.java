package com.k8s.springboot.ticket.service;

import com.k8s.springboot.ticket.entity.User;

public interface UserService {

    void saveUser(User user);

    User getUserByEmailAndPassword(User user);

    User getUserByEmail(String email);
}
