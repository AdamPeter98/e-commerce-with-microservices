package com.k8s.springboot.ticket.dto;

import java.io.Serializable;

public class currentUser implements Serializable {
    private Long id;
    private String email;
    private String iat = "12323423545";
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIat() {
        return iat;
    }
}
