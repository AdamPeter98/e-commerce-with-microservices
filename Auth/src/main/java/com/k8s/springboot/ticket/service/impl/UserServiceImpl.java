package com.k8s.springboot.ticket.service.impl;

import com.k8s.springboot.ticket.entity.User;
import com.k8s.springboot.ticket.repository.UserDao;
import com.k8s.springboot.ticket.service.UserService;
import com.k8s.springboot.ticket.util.PasswordEncrypterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public void saveUser(User user) {
        String hashedPassword = PasswordEncrypterUtil.passwordHash(user.getPassword());
        user.setPassword(hashedPassword);
        userDao.save(user);
    }

    @Override
    public User getUserByEmailAndPassword(User user) {
        String hashedPassword = PasswordEncrypterUtil.passwordHash(user.getPassword());
        user.setPassword(hashedPassword);
        return userDao.getUserByEmailAndPassword(user.getEmail(), user.getPassword());
    }

    @Override
    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }
}
