package com.k8s.springboot.ticket.facade;

import com.k8s.springboot.ticket.dto.Admin;
import com.k8s.springboot.ticket.dto.UserDto;
import com.k8s.springboot.ticket.entity.User;

public interface UserFacade {
    void signup(UserDto userDto);
    void signupAdmin(Admin admin);
    UserDto signIn(UserDto userDto);
    User findUserByEmail(String email);
    User findUserByEmailAndPassword(String email, String password);
}
