package com.k8s.springboot.ticket.repository;

import com.k8s.springboot.ticket.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserDao extends JpaRepository<User, Long> {
    User getUserByEmailAndPassword(String email, String password);

    User getUserByEmail(String email);
}
