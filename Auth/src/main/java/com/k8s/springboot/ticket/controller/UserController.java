package com.k8s.springboot.ticket.controller;

import com.k8s.springboot.ticket.dto.Admin;
import com.k8s.springboot.ticket.dto.UserDto;
import com.k8s.springboot.ticket.dto.currentUser;
import com.k8s.springboot.ticket.dto.response;
import com.k8s.springboot.ticket.facade.UserFacade;
import com.k8s.springboot.ticket.repository.UserDao;
import com.k8s.springboot.ticket.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private static final String TOKEN_KEY = "micro";

    private static final String ID_ATTRIBUTE_KEY = "userId";

    private JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();

    @Autowired
    private UserFacade userFacade;

    @Autowired
    private UserDao userDao;

    @GetMapping("/demo")
    public String demo() {
        return "heyho";
    }

    @PostMapping("/signup")
    public ResponseEntity signup(@RequestBody UserDto userDto, HttpServletResponse response) {
        if (userDao.getUserByEmail(userDto.getEmail()) != null) {
            return ResponseEntity.status(HttpStatus.IM_USED).body("Email in use");
        }
        if(checkForAdmin(userDto.getEmail(),userDto.getPassword())) {
            Admin admin = new Admin();
            admin.setEmail(userDto.getEmail());
            admin.setPassword(userDto.getPassword());
            admin.setRole("admin");
            userFacade.signupAdmin(admin);
        }else{
            userFacade.signup(userDto);
        }

        String token = jwtTokenUtil.generateToken(userDto.getEmail());
        Cookie cookie = new Cookie("micro", token);
        cookie.setPath("/");
        response.addCookie(cookie);
        return ResponseEntity.status(HttpStatus.CREATED).body(userDto);
    }

    @PostMapping("/admin/signup")
    public ResponseEntity signupAdmin(@RequestBody Admin admin, HttpServletResponse response) {
        if (userFacade.findUserByEmailAndPassword(admin.getEmail(),admin.getPassword()) != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User already exits");
        }

        System.out.println("signuuuup");

        userFacade.signupAdmin(admin);
        String token = jwtTokenUtil.generateToken(admin.getEmail());
        Cookie adminCookie = new Cookie("admin", "admin");
        adminCookie.setPath("/");
        response.addCookie(adminCookie);
        Cookie cookie = new Cookie("micro", token);
        cookie.setPath("/");
        response.addCookie(cookie);
        return ResponseEntity.status(HttpStatus.CREATED).body(admin);
    }

    @PostMapping("/signin")
    public ResponseEntity signin(@RequestBody UserDto userDto, HttpServletResponse response) {
        if (userFacade.findUserByEmailAndPassword(userDto.getEmail(),userDto.getPassword()) == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid credentials");
        }

        if (checkForAdmin(userDto.getEmail(), userDto.getPassword())) {

            Cookie cookie = new Cookie("admin", "admin");
            cookie.setPath("/");
            response.addCookie(cookie);
        }

        String token = jwtTokenUtil.generateToken(userDto.getEmail());
        Cookie cookie = new Cookie(TOKEN_KEY, token);
        cookie.setPath("/");

        response.addCookie(cookie);
        return ResponseEntity.status(HttpStatus.OK).body(userDto);
    }

    @PostMapping("/signout")
    public String signin(HttpServletResponse response) {
        Cookie adminCookie = new Cookie("admin", null);
        Cookie cookie = new Cookie(TOKEN_KEY, null);
        cookie.setMaxAge(0);
        cookie.setPath("/");
        adminCookie.setMaxAge(0);
        adminCookie.setPath("/");
        response.addCookie(adminCookie);
        response.addCookie(cookie);
        return "successful sign out";
    }

    @GetMapping("/currentuser")
    public ResponseEntity currentUser(HttpServletRequest request) {
        String tokenFromCookie = getTokenFromCookie(request.getCookies(), "micro");
        String adminFromCookie = getTokenFromCookie(request.getCookies(), "admin");

        System.out.println(tokenFromCookie + adminFromCookie);

        if (!tokenFromCookie.isBlank()) {
            response res = new response();
            currentUser currentUser = new currentUser();

            System.out.println("heeeeeeeeeeeeey"+userFacade.findUserByEmail(jwtTokenUtil.getEmailFromToken(tokenFromCookie)));

            currentUser.setEmail(userFacade.findUserByEmail(jwtTokenUtil.getEmailFromToken(tokenFromCookie)).getEmail());
            currentUser.setId(userFacade.findUserByEmail(jwtTokenUtil.getEmailFromToken(tokenFromCookie)).getId());

            System.out.println(request.getCookies());

            if (!adminFromCookie.isBlank()) {
                currentUser.setRole("admin");
            } else {
                currentUser.setRole("user");
            }

            res.setCurrentUser(currentUser);
            return ResponseEntity.status(HttpStatus.OK).body(res);
        }
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    private String getTokenFromCookie(Cookie[] cookies, String key) {
        String token = "";
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(key)) {
                    token = cookie.getValue();
                }
            }
        }
        System.out.println(token);
        return token;
    }

    private boolean checkForAdmin(String username, String password) {
        return username.equals(System.getenv("ADMIN_USERNAME")) && password.equals(System.getenv("ADMIN_PASSWORD"));
    }

}
