package com.k8s.springboot.ticket.facade.impl;

import com.k8s.springboot.ticket.dto.Admin;
import com.k8s.springboot.ticket.dto.UserDto;
import com.k8s.springboot.ticket.entity.User;
import com.k8s.springboot.ticket.facade.UserFacade;
import com.k8s.springboot.ticket.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserFacadeImpl implements UserFacade {

    @Autowired
    private UserService userService;


    @Override
    public void signup(UserDto userDto) {
        User user = new User();
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setRole("user");
        userService.saveUser(user);
    }

    @Override
    public void signupAdmin(Admin admin) {
        User user = new User();
        user.setEmail(admin.getEmail());
        user.setPassword(admin.getPassword());
        user.setRole(admin.getRole());
        userService.saveUser(user);
    }

    @Override
    public UserDto signIn(UserDto userDto) {
        User user = new User();
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        User userFormDb = userService.getUserByEmailAndPassword(user);
        UserDto userDto1 = new UserDto();
        userDto1.setEmail(userFormDb.getEmail());
        userDto1.setPassword(userFormDb.getPassword());
        return userDto1;
    }

    @Override
    public User findUserByEmail(String email) {
        return userService.getUserByEmail(email);
    }

    @Override
    public User findUserByEmailAndPassword(String email, String password) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
       return userService.getUserByEmailAndPassword(user);
    }
}
